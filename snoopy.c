/*
 * Define SNOOPY_LOGFILE to be the path to a file for logging the output,
 * or define SNOOPY_LOGDIR to be a directory in which one logfile will
 * be written for every process,
 * or the default will be taken, printing to STDERR
 */

#define _GNU_SOURCE

#include <dlfcn.h>
#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>

static FILE *logfh = NULL;
static pid_t pid;
static pid_t ppid;
static struct timestamp {
  unsigned int sec;
  unsigned int microsec;
} ts;

void get_logfile( void );
void epoch ( struct timestamp* );
void process_exit( void );
void __attribute__ ((constructor)) premain(); /* GnU: not portable */

extern char *__progname; /* GnU: not portable */

/*
 * These are the functions I wrap
 */
static int   ( *real_open ) ( const char *path, int oflag, mode_t mode ) = NULL;
static FILE *( *real_fopen   )( const char*, const char* );
static FILE *( *real_fopen64 )( const char*, const char* );
static int   ( *real_fclose )( FILE * );

#define GET_REAL( x, y ) \
  char *msg; \
  x = dlsym( RTLD_NEXT, y ); \
  if ( ( msg = dlerror() ) != NULL ) { \
    fprintf( stderr, "open: dlopen failed for 'y': %s\n", msg ); \
  }

#define GET_REAL_IF( x, y ) \
  if ( x == NULL ) { \
    GET_REAL( x, y ) \
  }

#define LOG( ... ) \
  if( logfh == NULL ) { get_logfile(); } \
  header(); \
  fprintf( logfh, __VA_ARGS__ )

#define DEBUG( ... ) fprintf( stderr, __VA_ARGS__ )

/*
 * Here starts the library
 */
void header( void ) {
  epoch( &ts );
  fprintf( logfh, "%u.%06u %d: ", ts.sec, ts.microsec, pid );
}

void __attribute__ ((constructor)) premain() { /* GnU: not portable */
  GET_REAL_IF( real_fopen, "fopen" )
  pid  = getpid();
  ppid = getppid();
  LOG( "PPID=%d prog='%s'\n", ppid, __progname );
  atexit( process_exit );
}

void process_exit( void ) {
// TW: This isn't getting called in docker containers
  LOG( "exit\n" );
}

void epoch ( struct timestamp *ts ) {
  struct timespec spec;
  clock_gettime(CLOCK_REALTIME, &spec);
  ts->sec       = spec.tv_sec;
  ts->microsec  = spec.tv_nsec / 1000;
}

void get_logfile( void ) {
  if ( logfh != NULL ) { return; }

// TW: This can get called before the PID is set, in containers?
// something to do with the difference between exec and fork?

  char *logenv = getenv( "SNOOPY_LOGFILE" );
  if ( logenv ) {
//  Can't call my own fopen, that assumes logfh is valid, which it isn't yet
    logfh = ( *real_fopen )( logenv, "a+" );
  } else {
    logenv = getenv( "SNOOPY_LOGDIR" );
    if ( logenv ) {
      char logdir[500]; // TW: Should really malloc, but who cares...
      sprintf( logdir, "%s/snoopy.%d.%d.log", logenv, pid, ppid );
      logfh = ( *real_fopen )( logdir, "a+" );
    } else {
      logfh = stderr;
    }
  }
}

/*
 * This is where the function interceptors start
 */

FILE *fopen64( const char *path, const char *mode ) {
  GET_REAL_IF( real_fopen64, "fopen64" )

  FILE *fh = ( *real_fopen64 )( path, mode );
  LOG( "fopen64(\"%s\", \"%s\") = 0x%08lx\n", path, mode, (uintptr_t)fh );
  return fh;
}

FILE *fopen( const char *path, const char *mode ) {
  GET_REAL_IF( real_fopen, "fopen" )

  FILE *fh = ( *real_fopen )( path, mode );
  LOG( "fopen(\"%s\", \"%s\") = 0x%08lx\n", path, mode, (uintptr_t)fh );
  return fh;
}

int fclose( FILE *fh ) {
  GET_REAL_IF( real_fclose, "fclose" )

  int status = ( *real_fclose )( fh );
  LOG( "fclose(0x%08lx) = %d\n", (uintptr_t)fh, status );
  return status;
}

int open( const char *path, int oflag, mode_t mode ) {
  GET_REAL_IF( real_open, "open" )

  int status = real_open(path, oflag, mode);
  LOG( "open(\"%s\", 0x%x, 0x%x) = %d\n", path, oflag, mode, status );
  return status;
}

// size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
// size_t fwrite( const void *buffer, size_t size, size_t count, FILE *stream );
