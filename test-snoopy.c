#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main( void ) {

  printf( "Calling the fopen() function...\n" );
  FILE *fd = fopen( "test-snoopy.sh", "r" );
  if ( ! fd ) {
    printf( "fopen() returned NULL\n" );
    return 1;
  }
  printf( "fopen() succeeded\n" );
  fclose( fd );

  FILE *fd1 = fopen( "snoopy.c", "r" );
  if ( ! fd1 ) {
    printf( "fopen() returned NULL\n" );
    return 1;
  }

  printf( "fopen() succeeded\n" );

  fclose( fd1 );

  fprintf(stderr, "This is a test.\n");
  int ret = open("somefile.log", 1);
  fprintf(stderr, "Return value is %d.\n", ret);
  
  return 0;
}
